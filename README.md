# My project's README

Showcasing a typical use case for Encodable and Decodable.

Using Generics for Request / Response mapping instantiated when needed in network queue.

Classic example for using operation queues, scalable to add more functions when needed to be fired on web service events.
