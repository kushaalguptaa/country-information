//
//  LandingVC.swift
//  CountyInfo
//
//  Created by Kushal on 1/24/18.
//  Copyright © 2018 Kushal. All rights reserved.
//

import Foundation

import UIKit

class LandingVC: UITableViewController, VCEventConsumer, UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate
{
    
    //Queue to execute City search fetch from preloaded data.
    var filterCounryOperation = OperationQueue()
    
    var objViewModel: LandingServiceManager = LandingServiceManager()
    var countryCollection: CountryCollection?
    
    let searchController = UISearchController(searchResultsController: nil)
    var filteredCountries: [Country]?
    
    override func viewDidLoad() {
        objViewModel.delegate = self
        objViewModel.getCountryData()
        
        //Setup Search controller
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        tableView.tableHeaderView = searchController.searchBar
        searchController.searchBar.delegate = self
        
        //Supression UI warning for collapsing Cell
        self.tableView.rowHeight = 44;
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        //Stopping any previous filters if running for recently typed text
        filterCounryOperation.cancelAllOperations()
        
        //Donot start filtering all data. Wait for atleast two characters to be typed.
        if let searchText: String = searchController.searchBar.text, searchText.count > 1 {
            filterCounryOperation.addOperation {
                
                //Filter data modals(Not for string containing) but actual match
                self.filteredCountries = self.countryCollection?.asArray.filter { country in
                    //Avoiding fatal search error
                    let compareString = country.name + country.id
                    if searchText.count > compareString.count
                    {
                        return false
                    }
                    
                    return compareString.lowercased().contains(searchText.lowercased())
                }
                
                //Updating Table on Main tread
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.countryCollection == nil ? 0 : (self.filteredCountries == nil ? self.countryCollection!.asArray.count : self.filteredCountries!.count))
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CountryCell = tableView.dequeueReusableCell(withIdentifier: "countryCell", for: indexPath as IndexPath) as! CountryCell
        
        let currentArray: [Country]? = filteredCountries != nil ? filteredCountries : countryCollection?.asArray
       
        if let countryObj: Country = currentArray?[indexPath.row]
        {
            cell.lblName.text = countryObj.name + " (" + countryObj.id + ")"

            if let flag = countryObj.flagImage
            {
                cell.imgFlag.isHidden = false
                cell.imageActivity.isHidden = true
                cell.imageActivity.stopAnimating()
                cell.imgFlag.image = flag
            }
            else    //Fetch Flag image for missing image in country collection data modal
            {
                cell.imgFlag.isHidden = true
                cell.imageActivity.isHidden = false
                cell.imageActivity.startAnimating()
                
                self.objViewModel.getFlagImageForCountryObj(obj: countryObj)
            }
        }
        else    //Default case while web service loads. Can be done graciously by having an activity view overlay!
        {
            cell.lblName.text = "Loading ..."
            cell.imgFlag.isHidden = true
            cell.imageActivity.isHidden = false
            cell.imageActivity.startAnimating()
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let vc: DetailsViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetailsVC") as? DetailsViewController
        {
            vc.currentCountry = filteredCountries == nil ? countryCollection?.asArray[indexPath.row] : filteredCountries![indexPath.row]
            resetSearchLayout()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //Resign responder if user tapped on Table
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        resetSearchLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func updateTableForCountryCollection(obj: CountryCollection?) {
        if obj != nil
        {
            self.countryCollection = obj
            DispatchQueue.main.sync {
                self.tableView.reloadData()
            }
        }
    }

    func getCountryCollection() -> CountryCollection? {
        return self.countryCollection
    }
    
    func updateFlagImageForCountryObj(obj: Country, flagImage: UIImage)
    {
        DispatchQueue.main.async {
            if self.countryCollection != nil
            {
                if let mainIndex = self.countryCollection?.asArray.index(of: obj)
                {
                    self.countryCollection?.asArray[mainIndex].flagImage = flagImage
                    if self.filteredCountries != nil
                    {
                        if let countryFilteredIndex = self.filteredCountries!.index(of: obj)
                        {
                            self.filteredCountries![countryFilteredIndex].flagImage = flagImage
                            self.tableView.reloadRows(at: [IndexPath(item: countryFilteredIndex, section: 0)], with: UITableViewRowAnimation.automatic)
                            return
                        }
                        
                    }
                    self.tableView.reloadRows(at: [IndexPath(item: mainIndex, section: 0)], with: UITableViewRowAnimation.automatic)
                }
            }
        }
    }
    
    @IBAction func searchTrigger(_ sender: Any) {
        tableView.contentOffset = CGPoint(x: 0, y: -(searchController.searchBar.frame.size.height + 20)) //adjusting status bar
        filteredCountries = nil
        self.searchController.searchBar.becomeFirstResponder()
        searchController.isActive = true             //Somehow keboard won't show if isActive was called before firstResponder
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        resetSearchLayout()
    }
    
    func resetSearchLayout()
    {
        if self.searchController.searchBar.isFirstResponder
        {
            searchController.isActive = false
            filteredCountries = nil
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            self.searchController.searchBar.endEditing(true)
        }
    }

}
