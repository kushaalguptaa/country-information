
//
//  LandingServiceManager.swift
//  CountyInfo
//
//  Created by Kushal on 1/25/18.
//  Copyright © 2018 Kushal. All rights reserved.
//

import Foundation
import UIKit

protocol VCEventConsumer: class {
    
    func updateTableForCountryCollection(obj: CountryCollection?)
    
    func updateFlagImageForCountryObj(obj: Country, flagImage: UIImage)

    func getCountryCollection() -> CountryCollection?
}

var flagQueue: DispatchQueue = DispatchQueue(label: "flags")

class LandingServiceManager {
    
    let imageBaseURL: String = "http://www.geonames.org/flags/l/"
    
    weak var delegate: VCEventConsumer?
    
    var networkManager = NetworkManager()
    
    func getCountryData() {
        //Get Country Data
        do{
            try networkManager.performRestNetworkRequestFor(requestCreatorSchema: CountryRequestHandler.self, responseModalSchema: CountryCollection.self, requestType: RequestType.GET, completion: { (countryObj) in
                self.delegate?.updateTableForCountryCollection(obj: countryObj)
            })
        }
        catch
        {
            
        }
    }
    
    func getFlagImageForCountryObj(obj: Country)
    {
        flagQueue.async {
            if let countryCollection: CountryCollection = self.delegate?.getCountryCollection()
            {
                if let indexOfCountry = countryCollection.asArray.index(of: obj)
                {
                    let imageURL: String = self.imageBaseURL + countryCollection.imageIDs[indexOfCountry].lowercased() + ".gif"
                    do{
                        try self.networkManager.fetchImageDataFor(url: imageURL, completion: { (flagImage) in
                            self.delegate?.updateFlagImageForCountryObj(obj: obj, flagImage: flagImage)
                        })
                    }
                    catch{
                        
                    }
                }
            }
        }
    }
}
