//
//  CountryRequestHandler.swift
//  CountyInfo
//
//  Created by Kushal on 1/25/18.
//  Copyright © 2018 Kushal. All rights reserved.
//

import Foundation

class CountryRequestHandler:NSObject, RequestCreator {
    
    required init(callingOperation: Errorable?) throws {
        
    }
    
    func getURLfor(baseURLString: String) -> URL? {
        let myURL = baseURLString + "/all"
        return URL(string: myURL)
    }
    
    func getHTTPType() -> String {
        return RequestType.GET.rawValue
    }
    
    func getRequestHeader() -> [String : String] {
        return [:]
    }
    
    func getRequestBody() -> [String : Any] {
        return[:]
    }
    
}
