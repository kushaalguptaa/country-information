//
//  Country.swift
//  CountyInfo
//
//  Created by Kushal on 1/25/18.
//  Copyright © 2018 Kushal. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

let NOT_AVAILABLE_PLACEHOLDER = "N.A"

class Country: NSObject, Decodable {
    
    var name: String
    var id: String
    var imageID: String
    var capital: String
    var population: Double?
    var area: Double?
    var nativeName: String
    var region: String
    var flagImage: UIImage?
    
    private var timezones: [String]
    var timezoneString: String
    {
        get
        {
            //Timezones not available sometimes
            if timezones.count > 0
            {
                var result: String = timezones[0]
                timezones.remove(at: 0)
                for obj in timezones
                {
                    result.append(", " + obj)
                }
                return result
            }
            else
            {
                return "NA"
            }
        }
    }
    
    //Storing responce data as private and processing to CLLocation only when needed
    private var latLng: [Double]
    private var _location: CLLocation?
    var location: CLLocation {
        get
        {
            //Co-ordinates not available for few countries
            if latLng.count == 2
            {
                return _location ?? CLLocation(latitude: latLng[0], longitude: latLng[1])
            }
            else
            {
                return CLLocation()
            }
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case name
        case id = "alpha3Code"
        case imageID = "alpha2Code"
        case capital = "capital"
        case population = "population"
        case area = "area"
        case nativeName = "nativeName"
        case region = "region"
        case timezones = "timezones"
        case latLng = "latlng"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.name = try values.decode(String.self, forKey: .name)
        self.id = try values.decode(String.self, forKey: .id)
        self.imageID = try values.decode(String.self, forKey: .imageID)
        self.capital = try values.decode(String.self, forKey: .capital)
        self.population = try values.decode(Double.self, forKey: .population)
        self.area = try values.decode(Double?.self, forKey: .area)
        self.nativeName = try values.decode(String.self, forKey: .nativeName)
        self.region = try values.decode(String.self, forKey: .region)
        self.timezones = try values.decode([String].self, forKey: .timezones)
        self.latLng = try values.decode([Double].self, forKey: .latLng)

    }
        
}
