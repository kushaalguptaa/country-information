//
//  CountryCollection.swift
//  CountyInfo
//
//  Created by Kushal on 1/25/18.
//  Copyright © 2018 Kushal. All rights reserved.
//

import Foundation

class CountryCollection:NSObject, Decodable {
    
    var asArray: [Country] = []             //For Landing table
    var asHash: [String: Country] = [:]     //Created for country lookup if needed.
    var imageIDs: [String] = []             //Segregating to be used for multithreaded image fetch
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.singleValueContainer()
        
        self.asArray = try values.decode([Country].self)
        
        for country in self.asArray
        {
            imageIDs.append(country.imageID)
        }
    }
        
}
