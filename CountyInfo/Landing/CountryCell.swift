//
//  CountryCell.swift
//  CountyInfo
//
//  Created by Kushal on 1/24/18.
//  Copyright © 2018 Kushal. All rights reserved.
//

import Foundation
import UIKit

class CountryCell: UITableViewCell {
    
    @IBOutlet weak var imgFlag: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imageActivity: UIActivityIndicatorView!
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
}
