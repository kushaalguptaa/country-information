//
//  BaseOperation.swift
//  CountyInfo
//
//  Created by Kushal on 1/24/18.
//  Copyright © 2018 Kushal. All rights reserved.
//

import Foundation

//Base class to enable the "Finish" method for an operation to continue in the queue
class BaseOperation: Operation
{
    var errorDescription: String?
    
    
    //    Overriding Operation properties for KVO(Key Value Observing) by the Operation Queue
    var _isExecuting = true
    
    override var isExecuting: Bool {
        get
        {
            return self._isExecuting
        }
    }
    
    var _isFinished = false
    
    override var isFinished: Bool {
        get
        {
            return self._isFinished
        }
    }
    
    
    class func keyPathsForValuesAffectingIsExecuting() -> Set<NSObject> {
        return ["state" as NSObject]
    }
    
    class func keyPathsForValuesAffectingIsFinished() -> Set<NSObject> {
        return ["state" as NSObject]
    }
    
    func finish()
    {
        self.willChangeValue(forKey: "state")
        self._isFinished = true
        self._isExecuting = false
        self.didChangeValue(forKey: "state")
    }
}
