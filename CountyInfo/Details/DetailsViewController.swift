//
//  ViewController.swift
//  CountyInfo
//
//  Created by Kushal on 1/24/18.
//  Copyright © 2018 Kushal. All rights reserved.
//

import UIKit
import MapKit

class DetailsViewController: UIViewController {
    
    var currentCountry: Country!

    @IBOutlet weak var lblView: UIScrollView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var flagView: UIImageView!
    
    @IBOutlet weak var nativeNameTag: UILabel!
    @IBOutlet weak var nativeName: UILabel!
    @IBOutlet weak var capitalTag: UILabel!
    @IBOutlet weak var capital: UILabel!
    @IBOutlet weak var regionTag: UILabel!
    @IBOutlet weak var region: UILabel!
    @IBOutlet weak var areaTag: UILabel!
    @IBOutlet weak var area: UILabel!
    @IBOutlet weak var populationTag: UILabel!
    @IBOutlet weak var population: UILabel!
    @IBOutlet weak var timezonesTag: UILabel!
    @IBOutlet weak var timezones: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Centering Map on selected country's location
        mapView.camera.centerCoordinate = currentCountry.location.coordinate
        //Trying to focus camera's zoom based on current country's area. Looking for alternatives
        mapView.camera.altitude = (currentCountry.area ?? 1).squareRoot()*1300
        
        self.title = currentCountry.name
        self.flagView.image = currentCountry.flagImage
        
        setLabelValues()
     
        setupConstrainsts()
    }
    
    func setLabelValues() {
        
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .decimal
        
        self.nativeName.text = currentCountry.nativeName
        self.nativeName.numberOfLines = 2
        self.capital.text = currentCountry.capital == "" ? "NA" : currentCountry.capital
        self.region.text = currentCountry.region
        //Formating Double to remove decimal and add separators
        self.area.text = currentCountry.area == nil ? NOT_AVAILABLE_PLACEHOLDER : formatter.string(from: currentCountry.area! as NSNumber)
        self.population.text = currentCountry.population == nil ? NOT_AVAILABLE_PLACEHOLDER : formatter.string(from: currentCountry.population! as NSNumber)
        self.timezones.text = currentCountry.timezoneString
        self.timezones.numberOfLines = 20
        
    }
    
    func setupConstrainsts()
    {
        
        //Size Map View
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.mapView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.mapView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 64).isActive = true
        self.mapView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        self.mapView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 1/3).isActive = true

        //Distance flag from Map
        self.flagView.translatesAutoresizingMaskIntoConstraints = false
        self.flagView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.flagView.topAnchor.constraint(equalTo: self.mapView.bottomAnchor, constant: 5).isActive = true
        self.flagView.widthAnchor.constraint(equalToConstant: 115).isActive = true
        self.flagView.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        let gapBelowFlag: Float = 20
        let tagLeftPadding: Float = 35

        //View containing labels
        self.lblView.translatesAutoresizingMaskIntoConstraints = false
        self.lblView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.lblView.topAnchor.constraint(equalTo: self.flagView.bottomAnchor, constant: CGFloat(gapBelowFlag)).isActive = true
        self.lblView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -20).isActive = true
        self.lblView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.lblView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.lblView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        self.lblView.layoutIfNeeded()
        
        //Process Labels
        self.nativeNameTag.sizeToFit()
        self.capitalTag.sizeToFit()
        self.regionTag.sizeToFit()
        self.areaTag.sizeToFit()
        self.timezonesTag.sizeToFit()
        self.populationTag.sizeToFit()
        let maxTagWidth = max(Float(nativeNameTag.frame.width), Float(capitalTag.frame.width), Float(regionTag.frame.width), Float(areaTag.frame.width), Float(timezonesTag.frame.width), Float(populationTag.frame.width))
        
        //label Native Name
        self.nativeNameTag.translatesAutoresizingMaskIntoConstraints = false
        self.nativeNameTag.leadingAnchor.constraint(equalTo: self.lblView.leadingAnchor, constant: CGFloat(tagLeftPadding)).isActive = true
        self.nativeNameTag.topAnchor.constraint(equalTo: self.lblView.topAnchor).isActive = true
        
        self.nativeName.translatesAutoresizingMaskIntoConstraints = false
        self.nativeName.leadingAnchor.constraint(equalTo: self.lblView.leadingAnchor, constant: CGFloat(maxTagWidth + tagLeftPadding + 15)).isActive = true
        self.nativeName.topAnchor.constraint(equalTo: self.lblView.topAnchor).isActive = true
        self.nativeName.trailingAnchor.constraint(equalTo: self.lblView.trailingAnchor, constant: -20).isActive = true
        self.nativeName.sizeToFit()
        //Not letting label break the right edge of the phone, pushing downwards for scroll view to kick in
        self.nativeName.preferredMaxLayoutWidth = self.nativeName.frame.size.width
        //Updating frame for layout for further label calculation for vertical spacing
        self.nativeName.layoutIfNeeded()

        //Have equally distributed space remaining below flag image. This can be done better by handling multi line labels and
        //aligning top anchor to bottom anchor of following component.
        
        //Label Capital
        self.spaceHorizontallyCurrentInfoLabel(current: capital, fromPrevious: nativeName)

        //Label Region
        self.spaceHorizontallyCurrentInfoLabel(current: region, fromPrevious: capital)

        //Label Area
        self.spaceHorizontallyCurrentInfoLabel(current: area, fromPrevious: region)

        //Label Timezone
        self.spaceHorizontallyCurrentInfoLabel(current: timezones, fromPrevious: area)

        //Label Population
        self.spaceHorizontallyCurrentInfoLabel(current: population, fromPrevious: timezones)

        let totalGap = lblView.frame.height - (nativeName.frame.height + capital.frame.height + region.frame.height + area.frame.height + timezones.frame.height + population.frame.height)
        //If components overlap or come as close as 2 pixels, put gap to 5 pixels ans enable scrolling
        let gapDistribution = totalGap > 12 ? totalGap/6 : 5
        
        //Vertically placing the info labels. Horizontally arranging tag labels according to the fitted info labels
        self.spaceVerticallyCurrentInfoLabel(current: capital, fromPrevious: nativeName, distance: gapDistribution)
        self.spaceVerticalCurrentTagLabel(current: capitalTag, fromInfoLabel: capital)
        self.spaceVerticallyCurrentInfoLabel(current: region, fromPrevious: capital, distance: gapDistribution)
        self.spaceVerticalCurrentTagLabel(current: regionTag, fromInfoLabel: region)
        self.spaceVerticallyCurrentInfoLabel(current: area, fromPrevious: region, distance: gapDistribution)
        self.spaceVerticalCurrentTagLabel(current: areaTag, fromInfoLabel: area)
        self.spaceVerticallyCurrentInfoLabel(current: timezones, fromPrevious: area, distance: gapDistribution)
        self.spaceVerticalCurrentTagLabel(current: timezonesTag, fromInfoLabel: timezones)
        self.spaceVerticallyCurrentInfoLabel(current: population, fromPrevious: timezones, distance: gapDistribution)
        self.spaceVerticalCurrentTagLabel(current: populationTag, fromInfoLabel: population)
        
        //Pushing scroll bottom anchor to increase content size vertically.
        self.population.bottomAnchor.constraint(equalTo: lblView.bottomAnchor, constant: -1 * gapDistribution).isActive = true

    }
    
    func spaceVerticallyCurrentInfoLabel(current: UILabel, fromPrevious: UILabel, distance: CGFloat)
    {
        current.topAnchor.constraint(equalTo: fromPrevious.bottomAnchor, constant: distance).isActive = true
    }

    
    func spaceHorizontallyCurrentInfoLabel(current: UILabel, fromPrevious: UILabel)
    {
        current.translatesAutoresizingMaskIntoConstraints = false
        current.leadingAnchor.constraint(equalTo: fromPrevious.leadingAnchor).isActive = true
        current.trailingAnchor.constraint(equalTo: self.lblView.trailingAnchor, constant: -20).isActive = true
        current.sizeToFit()
        //Not letting label break the right edge of the phone, pushing downwards for scroll view to kick in
        current.preferredMaxLayoutWidth = current.frame.size.width
        //Updating frame for layout for further label calculation for vertical spacing
        current.layoutIfNeeded()
    }
    
    func spaceVerticalCurrentTagLabel(current: UILabel, fromInfoLabel infoLbl: UILabel)
    {
        current.translatesAutoresizingMaskIntoConstraints = false
        current.leadingAnchor.constraint(equalTo: self.lblView.leadingAnchor, constant: 35).isActive = true
        current.topAnchor.constraint(equalTo: infoLbl.topAnchor).isActive = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

