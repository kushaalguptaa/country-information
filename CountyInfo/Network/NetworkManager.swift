//
//  NetworkManager.swift
//  CountyInfo
//
//  Created by Kushal on 1/24/18.
//  Copyright © 2018 Kushal. All rights reserved.
//

import Foundation
import UIKit

//Possible errors when trying to get data from API
enum RequestError: Error {
    case noResponseData
    case invalidDatModal
    case invalidAPIUrl
    case unknownError
    case parsingError
}

enum RequestType: String {
    case GET        = "GET"
    case POST       = "POST"
    case DELETE     = "DELETE"
    case QUERY      = "QUERY"
}

var apiBaseURL: String = "https://restcountries-v1.p.mashape.com"

//All requests are URL Query based. Just managing the thread with no URL method/type abstraction.
class NetworkManager {
    
    //One line Singleton
//    static var sharedinstance: NetworkManager = NetworkManager()
    
    //Full screen Activity Indicator can be added to Operation Queue
    var networkOperationQueue: OperationQueue = OperationQueue()
    
    var imageFetchQueue: OperationQueue = OperationQueue()
    
    var additionalOperations: [BaseOperation]
    
    init(additionalOperations: [BaseOperation] = []) {
        self.additionalOperations = additionalOperations
    }
    
    //Function for getting data from web services with query url
    func performQueryNetworkRequestForURL<RES>(url: String, query: [String: String], responseModalSchema: RES.Type, requestType: RequestType = RequestType.QUERY, completion: @escaping (_ responseModal: RES?) -> Void) throws -> Void where RES: Decodable
    {
        guard let neededURL: URL = URL(string: url) else
        {
            throw RequestError.invalidAPIUrl
        }

        //Cancel any existing network queries
        networkOperationQueue.cancelAllOperations()

        //Operation to fetch all related data with specifier query parameters
        let currentNetworkOperation = URLParamNetworkOperation(url: neededURL, query: query, responseModalSchema: responseModalSchema, completion: { (responseObj, error) in
            //Update data as needed
        })
        
        networkOperationQueue.addOperations([currentNetworkOperation], waitUntilFinished: false)
        networkOperationQueue.addOperations(self.additionalOperations, waitUntilFinished: false)

    }
    
    //Function to hit Rest API calls
    func performRestNetworkRequestFor<REQ, RES>(requestCreatorSchema: REQ.Type, responseModalSchema: RES.Type, requestType: RequestType = RequestType.QUERY, completion: @escaping (_ responseModal: RES?) -> Void) throws -> Void where REQ : RequestCreator, RES : Decodable
    {
        //Cancel any existing network queries
        networkOperationQueue.cancelAllOperations()
        
        var currentNetworkOperation: BaseOperation
        
        //Operation to fetch all related data with mentioned Rest service
        currentNetworkOperation = RestNetworkOperation(requestCreatorSchema: requestCreatorSchema, responseModalSchema: responseModalSchema, requestType: requestType, completion: { (responseObj, error) in
            if error != nil
            {
                //Handle error
            }
            else if responseObj != nil
            {
                completion(responseObj)
            }
        })
        
        networkOperationQueue.addOperations([currentNetworkOperation], waitUntilFinished: false)
        
    }
    
    func fetchImageDataFor(url: String, completion: @escaping (_ image: UIImage) -> Void) throws -> Void {
        //Operation to make second service call for completing country details data modal by fetching icon image
        guard let neededURL: URL = URL(string: url) else
        {
            throw RequestError.invalidAPIUrl
        }
        
        //Operation to fetch all related data with specifier query parameters
        let currentNetworkOperation = ImageFetchOperation(url: neededURL, completion: { (error, image) in
            if error != nil
            {
                //Handle Error
            }
            else if image != nil
            {
                completion(image!)
            }
        })
        
        //Queue current image fetch on top of existing service if any
        imageFetchQueue.addOperations([currentNetworkOperation], waitUntilFinished: false)

    }
    
}
