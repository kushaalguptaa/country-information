//
//  RestNetworkOperation.swift
//  CountyInfo
//
//  Created by Kushal on 1/24/18.
//  Copyright © 2018 Kushal. All rights reserved.
//

import Foundation

//Initialize first operation to get API response.
class RestNetworkOperation<REQ, RES>: BaseOperation, Errorable where REQ : RequestCreator, RES : Decodable {
    
    //From account registeration
    let appID = "1IosQYQKu0mshuIZjcqiIXbiLGJSp1dBB9Yjsnfd2aISWLA7Yk"
    
    var requestCreatorSchema: REQ.Type
    var responseModalSchema: RES.Type
    var requestType: RequestType
    var completion: (_ dataModal: RES?, _ error: RequestError?) -> Void
    
    init(requestCreatorSchema: REQ.Type, responseModalSchema: RES.Type, requestType: RequestType, completion: @escaping (_ dataModal: RES?, _ error: RequestError?) -> Void) {
        self.requestCreatorSchema = requestCreatorSchema
        self.responseModalSchema = responseModalSchema
        self.requestType = requestType
        self.completion = completion
    }
    
    override func start()
    {
        
        var requestCreator: RequestCreator!
        
        do
        {
             requestCreator = try self.requestCreatorSchema.init(callingOperation: self)
        }
        catch
        {
            self.completion(nil, RequestError.unknownError)
        }

        guard let url = requestCreator.getURLfor(baseURLString: apiBaseURL) else
        {
            self.completion(nil, RequestError.invalidAPIUrl)
            return
        }
        
        let request:NSMutableURLRequest = NSMutableURLRequest(url: url)
        request.httpMethod = requestCreator.getHTTPType()
        request.timeoutInterval = TimeInterval(requestCreator.getTimeout?() ?? 20.0)
        
        //Set Default values
        request.setValue(appID, forHTTPHeaderField: "X-Mashape-Key")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        //Set Request Header
        for i in requestCreator.getRequestHeader()
        {
            request.setValue(i.value, forHTTPHeaderField: i.key)
        }
        
        //TBD implement Encodable
        //Set Request Body
        if requestType != RequestType.GET
        {
            let uiRequestBody: [String: Any] = requestCreator.getRequestBody()
            var bodyData: Data?
            do {
                 bodyData = try JSONSerialization.data(withJSONObject: uiRequestBody, options: JSONSerialization.WritingOptions.init(rawValue: 0))
            }
            catch _ {
            }
            request.httpBody = bodyData
        }
        
        URLSession.shared.delegateQueue.cancelAllOperations()
        URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            if data == nil{
                self.completion(nil, RequestError.noResponseData)
            }
            else{
                do
                {
                    //Migrating to Codable
//                    let jsonData: Any = try JSONSerialization.jsonObject(with: data!, options: [])
//                    let dataModal = try self.responseModalSchema //self.responseModalSchema.init(jsonData: data!, callingOperation: self)
                    
                    let decoder = JSONDecoder()
                    let dataModal = try decoder.decode(self.responseModalSchema, from: data!)

                    self.completion(dataModal, nil)
                }
                catch let error
                {
                    print(error)
                    if error is DecodingError
                    {
                        self.completion(nil, RequestError.parsingError)
                    }
                    
                    self.completion(nil, RequestError.unknownError)

                }
            }
            
            self.finish()
        }).resume()
    }
}
