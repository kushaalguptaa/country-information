//
//  JSONMappable.swift
//  CountyInfo
//
//  Created by Kushal on 1/24/18.
//  Copyright © 2018 Kushal. All rights reserved.
//

import Foundation

//Protocol enabling data Modals
//protocol JSONMappable {
//
//    init(jsonData: Data, callingOperation: Errorable?) throws
//
////    func mapValueFor(key: String, value: Any)
//
//}

@objc protocol RequestCreator {
    
    init(callingOperation: Errorable?) throws
    
    func getURLfor(baseURLString: String) -> URL?
    
    func getHTTPType() -> String
    
    func getRequestHeader() -> [String:String]
    
    func getRequestBody() -> [String: Any]
    
    @objc optional func getTimeout() -> Float
    
}

//Operations can implement to get errors out of competion blocks to throw
@objc protocol Errorable {
    
}
