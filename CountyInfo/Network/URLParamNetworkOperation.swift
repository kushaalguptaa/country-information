//
//  NetworkOperation.swift
//  CountyInfo
//
//  Created by Kushal on 1/24/18.
//  Copyright © 2018 Kushal. All rights reserved.
//

import Foundation

//Initialize first operation to get API response.
class URLParamNetworkOperation<RES>: BaseOperation, Errorable where RES: Decodable {
    
    var networkURL: URL
    var responseModalSchema: RES.Type
    var requestQuery: [String: String]
    var completion: (_ dataModal: RES?, _ error: RequestError?) -> Void

    init(url: URL, query: [String:String], responseModalSchema: RES.Type, completion: @escaping (_ dataModal: RES?, _ error: RequestError?) -> Void) {
        self.networkURL = url
        self.responseModalSchema = responseModalSchema
        self.requestQuery = query
        self.completion = completion
    }
    
    override func start()
    {
        let components = NSURLComponents(url: networkURL, resolvingAgainstBaseURL: true)
        var query: [URLQueryItem] = []
        
        //Adding Authentication Query(App ID)
//        query.append(URLQueryItem(name: "APPID", value: appID))
        
        //Adding Additional Query options
        for (key, value) in requestQuery
        {
             query.append(URLQueryItem(name: key, value: value))
        }
        components?.queryItems = query
        
        let request: URLRequest = URLRequest(url: (components?.url)!)
        
        URLSession.shared.delegateQueue.cancelAllOperations()
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            if data == nil{
                self.completion(nil, RequestError.noResponseData)
            }
            else{
                do
                {
//                    let jsonData: Any = try JSONSerialization.jsonObject(with: data!, options: [])
//                    let dataModal = try (self.responseModalSchema as? JSONMappable.Type)?.init(jsonData: jsonData as! Data, callingOperation: self)
                    
                    let decoder = JSONDecoder()
                    let dataModal = try decoder.decode(self.responseModalSchema, from: data!)
                    
                    self.completion(dataModal, nil)
                }
                catch
                {
                    self.completion(nil, RequestError.unknownError)
                }
            }
            
            self.finish()
        }).resume()
    }    
}
