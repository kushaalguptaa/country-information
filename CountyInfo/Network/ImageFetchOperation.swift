//
//  ImageFetchOperation.swift
//  CountyInfo
//
//  Created by Kushal on 1/24/18.
//  Copyright © 2018 Kushal. All rights reserved.
//

import Foundation

import UIKit

class ImageFetchOperation: BaseOperation, Errorable
{
    var completion: (_ error: RequestError?, _ image: UIImage?) -> Void
    var imageURL: URL
    
    init(url: URL, completion: @escaping (_ error: RequestError?, _ image: UIImage?) -> Void) {
        self.completion = completion
        self.imageURL = url
    }
    
    override func start() {
        
        URLSession.shared.dataTask(with: imageURL, completionHandler: { (data, response, error) in

            if data == nil
            {
                self.completion(RequestError.noResponseData, nil)
                //Handle Error
            }
            else
            {
                self.completion(nil, UIImage(data: data!))
            }

            self.finish()
        }).resume()

    }
}
